# xmldiff

deep diffing xml with arbitrary ordering

# Execution

## Binary

A binary file that was prepared on Windows with [pyinstaller](https://www.pyinstaller.org/) is located in folder `dist` of this repository.

```
xmldiff.exe "path/to/left.xml" "path/to/right.xml"
```

## From source

1. Clone repository
1. Open a command-line with python in the cloned repository
1. Create virtual environment `python3 -m venv venv`
1. Install dependencies `pip install -r requirements.txt`
1. `python xmldiff.py "path/to/left.xml" "path/to/right.xml"`


Licensed with MIT