import json
import xmltodict
import deepdiff
import pprint
import sys

def main():
    left_xml = {}
    right_xml = {}

    left_file_path = sys.argv[1]
    right_file_path = sys.argv[2]

    print("Reading left: " + left_file_path)
    with open(left_file_path, 'r', encoding='utf-8') as left_xml_file:
        left_xml = left_xml_file.read()

    print("Reading right: " + right_file_path)
    with open(right_file_path, 'r', encoding='utf-8') as right_xml_file:
        right_xml = right_xml_file.read()

    print("To dict left...")
    left_dict = xmltodict.parse(left_xml)

    print("To dict right...")
    right_dict = xmltodict.parse(right_xml)

    print("Diffing left vs. right...")
    diff = deepdiff.DeepDiff(left_dict, right_dict, ignore_order=True)

    pp = pprint.PrettyPrinter(4)
    print("Diff:")
    pp.pprint(diff)

if __name__ == "__main__":
    main()